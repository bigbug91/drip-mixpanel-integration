﻿using System.Collections.Generic;

namespace Models
{
    public class DripMixPanelRule
    {
        public string DripEventName { get; set; }

        public string MixPanelEventName { get; set; }

        public List<DripMixPanelFieldMapping> Fields { get; set; }

        public DripMixPanelRule(string dripEventName, string mixPanelEventName)
        {
            DripEventName = dripEventName;
            MixPanelEventName = mixPanelEventName;
        }
    }

    public class DripMixPanelFieldMapping
    {
        public string DripFieldPath { get; set; }

        public string MixPanelFieldName { get; set; }

        public DripMixPanelFieldMapping(string dripFieldPath, string mixpanelFieldName)
        {
            DripFieldPath = dripFieldPath;
            MixPanelFieldName = mixpanelFieldName;
        }
    }
}
