﻿using System;

namespace Models
{
    public class DripEvent
    {
        public string Event { get; set; }

        public DateTime OccurredAt { get; set; }

        public dynamic Data { get; set; }
    }
}