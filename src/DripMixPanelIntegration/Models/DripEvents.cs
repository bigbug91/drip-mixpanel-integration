﻿namespace Services
{
    public static class DripEvents
    {
        public static class Subscriber
        {
            public const string created = "subscriber.created";
            public const string deleted = "subscriber.deleted";
            public const string marked_as_deliverable = "subscriber.marked_as_deliverable";
            public const string marked_as_undeliverable = "subscriber.marked_as_undeliverable";
            public const string subscribed_to_campaign = "subscriber.subscribed_to_campaign";
            public const string removed_from_campaign = "subscriber.removed_from_campaign";
            public const string unsubscribed_from_campaign = "subscriber.unsubscribed_from_campaign";
            public const string unsubscribed_all = "subscriber.unsubscribed_all";
            public const string reactivated = "subscriber.reactivated";
            public const string completed_campaign = "subscriber.completed_campaign";
            public const string applied_tag = "subscriber.applied_tag";
            public const string removed_tag = "subscriber.removed_tag";
            public const string updated_custom_field = "subscriber.updated_custom_field";
            public const string updated_email_address = "subscriber.updated_email_address";
            public const string updated_lifetime_value = "subscriber.updated_lifetime_value";
            public const string updated_time_zone = "subscriber.updated_time_zone";
            public const string received_email = "subscriber.received_email";
            public const string opened_email = "subscriber.opened_email";
            public const string clicked_email = "subscriber.clicked_email";
            public const string bounced = "subscriber.bounced";
            public const string complained = "subscriber.complained";
            public const string clicked_trigger_link = "subscriber.clicked_trigger_link";
            public const string visited_page = "subscriber.visited_page";
            public const string became_lead = "subscriber.became_lead";
            public const string became_non_prospect = "subscriber.became_non_prospect";
            public const string updated_lead_score = "subscriber.updated_lead_score";
            public const string performed_custom_event = "subscriber.performed_custom_event";
            public const string updated_alias = "subscriber.updated_alias";
        }
    }
}