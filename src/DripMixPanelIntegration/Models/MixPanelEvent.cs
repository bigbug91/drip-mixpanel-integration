﻿namespace Models
{
    public class MixPanelEvent
    {
        public string Event { get; set; }

        public dynamic Properties { get; set; }
    }
}