﻿using System.Threading.Tasks;
using Models;

namespace Services
{
    public interface IMixPanelClient
    {
        Task<int> Track(MixPanelEvent mixPanelEvent);
    }
}