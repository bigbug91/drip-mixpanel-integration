﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Services
{
    public class MixPanelClient : IMixPanelClient
    {
        private readonly string _token;
        private readonly HttpClient _client;
        private readonly JsonSerializerSettings _settings;

        public MixPanelClient() : this("0fd163b9d9580598e7c5ab73ce08b086")
        {
        }

        public MixPanelClient(string token)
        {
            _token = token;
            _client = new HttpClient { BaseAddress = new Uri("https://api.mixpanel.com/") };
            _settings = new JsonSerializerSettings()
            {
                ContractResolver = new DefaultContractResolver()
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                }
            };
        }

        public async Task<int> Track(MixPanelEvent mixPanelEvent)
        {
            mixPanelEvent.Properties.token = _token;

            var jsonStr = JsonConvert.SerializeObject(mixPanelEvent, _settings);
            var jsonStrBytes = Encoding.UTF8.GetBytes(jsonStr);
            var jsonBase64 = Convert.ToBase64String(jsonStrBytes);

            var res = await _client.GetAsync($"track?data={jsonBase64}");
            var resBody = await res.Content.ReadAsStringAsync();

            return int.Parse(resBody);
        }
    }
}