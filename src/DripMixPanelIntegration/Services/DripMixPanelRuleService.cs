﻿using System.Collections.Generic;
using System.Linq;
using Models;

namespace Services
{
    public class DripMixPanelRuleService : IDripMixPanelRuleService
    {
        private readonly List<DripMixPanelRule> _rules;

        public DripMixPanelRuleService()
        {
            _rules = new List<DripMixPanelRule>();

            _rules.Add(new DripMixPanelRule(DripEvents.Subscriber.created, "Drip New Subscriber")
            {
                Fields = new List<DripMixPanelFieldMapping>()
                {
                    new DripMixPanelFieldMapping("data.subscriber.email", "Drip Email"),
                    new DripMixPanelFieldMapping("data.subscriber.tags", "Drip Tags"),
                }
            });

            _rules.Add(new DripMixPanelRule(DripEvents.Subscriber.applied_tag, "Drip New Subscriber Tag")
            {
                Fields = new List<DripMixPanelFieldMapping>()
                {
                    new DripMixPanelFieldMapping("data.subscriber.email", "Drip Email"),
                    new DripMixPanelFieldMapping("data.subscriber.tags", "Drip Tags"),
                    new DripMixPanelFieldMapping("data.properties.tag", "Drip New Tag"),
                }
            });

            _rules.Add(new DripMixPanelRule(DripEvents.Subscriber.clicked_trigger_link, "Drip New Trigger Link Click")
            {
                Fields = new List<DripMixPanelFieldMapping>()
                {
                    new DripMixPanelFieldMapping("data.subscriber.email", "Drip Email"),
                    new DripMixPanelFieldMapping("data.subscriber.tags", "Drip Tags"),
                    new DripMixPanelFieldMapping("data.properties.url", "Drip Link URL"),
                }
            });


            _rules.Add(new DripMixPanelRule(DripEvents.Subscriber.completed_campaign, "Drip Completes Campaign")
            {
                Fields = new List<DripMixPanelFieldMapping>()
                {
                    new DripMixPanelFieldMapping("data.subscriber.email", "Drip Email"),
                    new DripMixPanelFieldMapping("data.subscriber.tags", "Drip Tags"),
                    new DripMixPanelFieldMapping("data.properties.campaign_name", "Drip Campaign Name"),
                }
            });

            //Drip New Email OPen Drip Email, Drip Email Subject, Drip Tags, Drip Created At, Drip Time Zone
            _rules.Add(new DripMixPanelRule(DripEvents.Subscriber.opened_email, "Drip New Email Open")
            {
                Fields = new List<DripMixPanelFieldMapping>()
                {
                    new DripMixPanelFieldMapping("data.subscriber.email", "Drip Email"),
                    new DripMixPanelFieldMapping("data.subscriber.tags", "Drip Tags"),
                    new DripMixPanelFieldMapping("occurred_at", "Drip Created At"),
                    new DripMixPanelFieldMapping("data.properties.email_subject", "Drip Email Subject"),
                    //new DripMixPanelFieldMapping("data.properties.email_subject", "Drip Time Zone"), ???
                }
            });

            //Drip Remove Subscriber Tag  Drip Email, Drip Removed Tag, Drip Tags
            _rules.Add(new DripMixPanelRule(DripEvents.Subscriber.removed_tag, "Drip Remove Subscriber Tag")
            {
                Fields = new List<DripMixPanelFieldMapping>()
                {
                    new DripMixPanelFieldMapping("data.subscriber.email", "Drip Email"),
                    new DripMixPanelFieldMapping("data.subscriber.tags", "Drip Tags"),
                    new DripMixPanelFieldMapping("data.properties.tag", "Drip Removed Tag"),
                }
            });

            //Drip New Email Click Drip Email, Drip Email Subject, Drip Properties URL, Drip Tags
            //Drip New Email Click Drip Email, Drip Email Subject, Drip Properties URL, Drip Tags
            _rules.Add(new DripMixPanelRule(DripEvents.Subscriber.clicked_email, "Drip New Email Click")
            {
                Fields = new List<DripMixPanelFieldMapping>()
                {
                    new DripMixPanelFieldMapping("data.subscriber.email", "Drip Email"),
                    new DripMixPanelFieldMapping("data.subscriber.tags", "Drip Tags"),
                    new DripMixPanelFieldMapping("data.properties.email_subject", "Drip Email Subject"),
                    new DripMixPanelFieldMapping("data.properties.url", "Drip URL"),
                }
            });

            //Drip New Global Unsubscribe Drip Email, Drip Tags, Drip Created At
            _rules.Add(new DripMixPanelRule(DripEvents.Subscriber.unsubscribed_all, "Drip New Global Unsubscribe")
            {
                Fields = new List<DripMixPanelFieldMapping>()
                {
                    new DripMixPanelFieldMapping("data.subscriber.email", "Drip Email"),
                    new DripMixPanelFieldMapping("data.subscriber.tags", "Drip Tags"),
                    new DripMixPanelFieldMapping("occurred_at", "Drip Created At"),
                }
            });
        }

        public IEnumerable<DripMixPanelRule> GetList()
        {
            return _rules;
        }

        public IEnumerable<DripMixPanelRule> GetList(string dripEventName)
        {
            return _rules.Where(r => r.DripEventName == dripEventName);
        }
    }
}