﻿using System;
using System.Collections.Generic;
using Models;

namespace Services
{
    public interface IDripMixPanelRuleService
    {
        IEnumerable<DripMixPanelRule> GetList();

        IEnumerable<DripMixPanelRule> GetList(string dripEventName);
    }
}
