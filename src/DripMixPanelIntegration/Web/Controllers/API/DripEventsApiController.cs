﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.Extensions.Logging;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Services;

namespace Web.Controllers.API
{
    [Route("api/v1/drip/events")]
    [ApiController]
    public class DripEventsApiController : ControllerBase
    {
        private readonly ILogger<DripEventsApiController> _logger;
        private readonly IMixPanelClient _mixPanelClient;
        private readonly IDripMixPanelRuleService _ruleService;

        public DripEventsApiController(ILogger<DripEventsApiController> logger, IMixPanelClient mixPanelClient, IDripMixPanelRuleService ruleService)
        {
            _logger = logger;
            _mixPanelClient = mixPanelClient;
            _ruleService = ruleService;
        }

        [HttpPost, Route("")]
        public async Task<IActionResult> Post([FromBody] JToken body)
        {
            if (body == null)
                return BadRequest("Missing request body.");

            var matchingRules = _ruleService.GetList(body["event"].Value<string>()).ToArray();

            foreach (var rule in matchingRules)
            {
                dynamic properties = BuildMixPanelData(body, rule);

                await _mixPanelClient.Track(new MixPanelEvent()
                {
                    Event = rule.MixPanelEventName,
                    Properties = properties
                });
            }

            return Ok();
        }

        private object BuildMixPanelData(JToken dripEvent, DripMixPanelRule rule)
        {
            var eo = new ExpandoObject();
            var eoColl = (ICollection<KeyValuePair<string, object>>) eo;

            foreach (var fieldMapping in rule.Fields)
            {
                var fieldPaths = fieldMapping.DripFieldPath.Split('.');
                var value = dripEvent;

                foreach (var field in fieldPaths)
                {
                    value = value[field];
                }

                eoColl.Add(new KeyValuePair<string, object>(fieldMapping.MixPanelFieldName, value));
            }

            dynamic eoDynamic = eo;

            return eoDynamic;
        }
    }
}