﻿using Microsoft.AspNetCore.Mvc;
using Services;

namespace Web.Controllers.API
{
    [Route("api/v1/rules")]
    [ApiController]
    public class RulesApiController : ControllerBase
    {
        private readonly IDripMixPanelRuleService _ruleService;

        public RulesApiController(IDripMixPanelRuleService ruleService)
        {
            _ruleService = ruleService;
        }

        [HttpGet, Route("")]
        public IActionResult GetList()
        {
            var rules = _ruleService.GetList();
            return Ok(rules);
        }
    }
}