﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers.API
{
    [Route("api/v1/test")]
    [ApiController]
    public class TestApiController : ControllerBase
    {
        [HttpGet, Route((""))]
        public IActionResult Get()
        {
            return Ok(new
            {
                messag = "hello there."
            });
        }
    }
}