﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Services;
using Web.Models;

namespace Web.Controllers
{
    public class RulesController : Controller
    {
        private readonly IDripMixPanelRuleService _ruleService;

        public RulesController(IDripMixPanelRuleService ruleService)
        {
            _ruleService = ruleService;
        }

        public IActionResult Index()
        {
            var vm = new RuleListViewModel()
            {
                Rules = _ruleService.GetList().ToList()
            };

            return View(vm);
        }
    }
}