using System.Collections.Generic;
using Models;

namespace Web.Models
{
    public class RuleListViewModel
    {
        public IList<DripMixPanelRule> Rules { get; set; }
    }
}